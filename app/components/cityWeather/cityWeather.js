app.controller("cityWeatherCtrl", function ($scope, $http) {
    var getCitiesUrl = sampleDataEnabled ? "./sampleData/cities.json" :
        "http://api.openweathermap.org/data/2.5/group?id=2759794,2950159,2643743,2988507,756135&units=metric" +
        "&appid=" + appid;

    $scope.cities = [];

    $http.get(getCitiesUrl).then(function (response) {
        response.data.list.forEach(function (city) {
            $scope.cities.push({
                id: city.id,
                name: city.name,
                weather: city.weather[0].description,
                temperature: city.main.temp,
                wind: city.wind.speed
            });
        });
    }, function () {
        this.handleRequestError();
    });

    $scope.orderByMe = function (orderBy) {
        $scope.myOrderBy = orderBy;
    };

    $scope.showAdditionalInfo = function (cityId) {
        var getCityUrl = sampleDataEnabled ? "./sampleData/city.json" : "http://api.openweathermap.org/data/2.5/forecast?id="
            + cityId + "&units=metric&appid=" + appid;

        $http.get(getCityUrl).then(function (response) {
            $scope.graph = {
                name: response.data.city.name,
                data: [[], []],
                labels: [],
                series: ["Temperature [℃]", "Wind [meter/sec]"],
                datasetOverride: [{yAxisID: "temperature"}, {yAxisID: "wind"}],
                options: {
                    legend: {
                        display: true
                    },
                    scales: {
                        yAxes: [
                            {
                                id: "temperature",
                                type: "linear",
                                display: true,
                                position: "left"
                            },
                            {
                                id: "wind",
                                type: "linear",
                                display: true,
                                position: "right"
                            }
                        ]
                    }
                }
            };
            response.data.list.forEach(function (forecast) {
                $scope.graph.labels.push(forecast.dt_txt);
                $scope.graph.data[0].push(forecast.main.temp);
                $scope.graph.data[1].push(forecast.wind.speed);
            });
            $scope.showCityInfo = true;
        }, function () {
            this.handleRequestError();
        });
    };
});

function handleRequestError() {
    alert("Something went wrong");
}