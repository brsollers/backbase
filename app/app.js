// app properties
var sampleDataEnabled = false;
var appid = "9097b7dd1e3e7a881188b1bc9abea361";

var app = angular.module("myApp", ["ngRoute", "chart.js"]);

// routes configuration
app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "components/cityWeather/cityWeather.html",
            controller  : "cityWeatherCtrl"
        })
        .otherwise({redirectTo: "/"});
});
